
----------------------------------------------------------------------------
--         LanathelQueue
--     Copyright (C) 2020  Ritual
-- 
--     This program is free software: you can redistribute it and/or modify
--     it under the terms of the GNU General Public License as published by
--     the Free Software Foundation, either version 3 of the License, or
--     (at your option) any later version.
-- 
--     This program is distributed in the hope that it will be useful,
--     but WITHOUT ANY WARRANTY; without even the implied warranty of
--     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--     GNU General Public License for more details.
-- 
--     You should have received a copy of the GNU General Public License
--     along with this program.  If not, see <http://www.gnu.org/licenses/>.
--     
-----------------------------------------------------------------------------

local function AddMsg(text)
  DEFAULT_CHAT_FRAME:AddMessage("|c00ff0000LanathelQueue|r " .. (text and tostring(text) or "nil"))
end

local leftoverList = {}
local raid = {}

---------------------
--       GUI       --
---------------------

LQ = {
  Revision = ("$Revision: 0 $"):sub(12, -3),
  Version = "1.9",
  DisplayVersion = "1.9",
  mainframe = {width = 450, height = 330}, 
  playerframe = {width = 65, height = 15}}

LQ.DefaultOptions = {
  minimapIcon = true, 
  share = true, 
  minimapPos = {x = -110, y = -70}, 
  dataSource = "auto",
  diff = 3,
  priorityList = {}}

LQ_SavedOptions = {}

local backdrop = {
  bgFile = "Interface\\AddOns\\LanathelQueue\\img\\background", tile = true, tileSize = 8,
  edgeFile = "Interface\\AddOns\\LanathelQueue\\img\\border", edgeSize = 8,
  insets = {left = 0, right = 0, top = 0, bottom = 0},
}

local backdrop_noborder = {
  bgFile = "Interface\\AddOns\\LanathelQueue\\img\\background", tile = true, tileSize = 8,
  insets = {left = 0, right = 0, top = 0, bottom = 0},
}

local function makeTexture(frame, path, blend)
  local t = frame:CreateTexture()
  t:SetAllPoints(frame)
  t:SetTexture(path)
  if blend then
    t:SetBlendMode(blend)
  end
  return t
end

local function checkEntry(t, val)
  for i, v in ipairs(t) do
      if v == val then
          return i
      end
  end
  return false
end

local function addDefaultOptions(t1, t2)
  for i, v in pairs(t2) do
      if t1[i] == nil then
          t1[i] = v
      elseif type(v) == "table" then
          addDefaultOptions(v, t2[i])
      end
  end
end

local function loadOptions()
  LQ.Options = LQ_SavedOptions
  addDefaultOptions(LQ.Options, LQ.DefaultOptions)
end

local lqGUI = CreateFrame("Frame",nil,UIParent) -- окно аддона
lqGUI:Hide()
lqGUI.isOpen = false
lqGUI:SetFrameStrata("DIALOG")
lqGUI:SetWidth(LQ.mainframe.width)
lqGUI:SetHeight(LQ.mainframe.height)
lqGUI:SetBackdrop(backdrop)
lqGUI:SetBackdropColor(0,0,0,.85)
lqGUI:SetPoint("CENTER",0,0)
lqGUI:SetMovable(true)
lqGUI:EnableMouse(true)
lqGUI:SetScript("OnMouseDown",function()
    lqGUI:StartMoving()
  end)
lqGUI:SetScript("OnMouseUp",function()
    lqGUI:StopMovingOrSizing()
  end)
lqGUI:RegisterEvent("ADDON_LOADED")

local function configure()
  if LQ.Options.minimapIcon then
    lqGUI.options.minimapCB:SetChecked(true)
    lqGUI.button:Show()
  else 
    lqGUI.button:Hide()
  end
  lqGUI.options.Diff10:SetChecked(LQ.Options.diff == 3)
  lqGUI.options.Diff25:SetChecked(LQ.Options.diff == 4)
  if LQ.Options.dataSource == "auto" then
    lqGUI.options.source:SetChecked(false)
    lqGUI.options.sourceRecount:Hide()
    lqGUI.options.sourceDetails:Hide()
    lqGUI.options.sourceSkada:Hide()
  else
    lqGUI.options.source:SetChecked(true)
    lqGUI.options.sourceRecount:Show()
    lqGUI.options.sourceDetails:Show()
    lqGUI.options.sourceSkada:Show()
  end
  lqGUI.options.sourceRecount:SetChecked((LQ.Options.dataSource == "Recount") and lqGUI.options.source:GetChecked())
  lqGUI.options.sourceDetails:SetChecked((LQ.Options.dataSource == "Details") and lqGUI.options.source:GetChecked())
  lqGUI.options.sourceSkada:SetChecked((LQ.Options.dataSource == "Skada") and lqGUI.options.source:GetChecked())
end

lqGUI.resFuncs = {}

function lqGUI.resFuncs:auto()
  local tbl = {}
  for i=1,GetNumRaidMembers() do
    tbl[#tbl+1] = UnitName("raid"..i)
  end
  return tbl
end

function lqGUI.resFuncs:Recount()
  if Recount then
    local tbl = {}
    for i,v in ipairs(Recount.MainWindow.DispTableSorted) do
      tbl[i] = v[1]
    end
    return tbl
  else
    AddMsg("Recount not found, switching back to auto")
    LQ.Options.dataSource = "auto"
    configure()
    return lqGUI.resFuncs:auto()
  end
end

function lqGUI.resFuncs:Details()
  if Details then
    local com = Details:GetInstance(1):GetShowingCombat()[1]._ActorTable
    local t, t1 = {}, {}
    for i,v in pairs(com) do 
      t[v.last_dps] = v.nome
      t1[#t1+1] = v.last_dps
    end 
    table.sort(t1)
    tbl = {}
    for i,v in ipairs(t1) do
      tbl[#t1+1-i] = t[v]
    end
    return tbl
  else
    AddMsg("Details not found, switching back to auto")
    LQ.Options.dataSource = "auto"
    configure()
    return lqGUI.resFuncs:auto()
  end
end

function lqGUI.resFuncs:Skada()
  if Skada then
    local wins = Skada:GetWindows()
    local tbl = {}
    for _,w in pairs(wins) do
      if tostring(w.selectedmode) == "Skada_Damage" then
        for i,v in ipairs(w.dataset) do
          tbl[i] = v.label
        end
        break
      end
    end
    return tbl
  else
    AddMsg("Skada not found, switching back to auto")
    LQ.Options.dataSource = "auto"
    configure()
    return lqGUI.resFuncs:auto()
  end
end
  
lqGUI:SetScript("OnEvent", function(self, event, arg1)
  if event == "ADDON_LOADED" then
    if arg1 == "LanathelQueue" then
      loadOptions()
      configure()
      self:UpdateRoster()
      if not LQ.Options.priorityList then
        self:GetDefaultPositions()
      end
      self:UpdatePositions()
      self:RegisterEvent("RAID_ROSTER_UPDATE")
      AddMsg(LQ.Version .. " loaded")
    end
  elseif event == "RAID_ROSTER_UPDATE" then
    self:UpdateRoster()
    self:UpdatePositions()
  end
end)
  
-- lqGUI.ricardo = lqGUI:CreateTexture()
-- lqGUI.ricardo:SetWidth(lqGUI:GetWidth()-6-20-LQ.playerframe.width-5)
-- lqGUI.ricardo:SetHeight(lqGUI:GetHeight()-41)
-- lqGUI.ricardo:SetPoint("TOPLEFT", 3, -38)
-- lqGUI.ricardo:SetTexture("Interface\\AddOns\\LanathelQueue\\img\\ricardo\\tile00")
-- lqGUI.ricardo:SetAlpha(0.8)
-- local kk = 0
-- local tt = GetTime()
-- lqGUI:SetScript("OnUpdate",function(self)
--   if GetTime() - tt > 30 then
--     lqGUI.ricardo:SetTexture("Interface\\AddOns\\LanathelQueue\\img\\ricardo\\tile0" .. kk)
--     kk = kk + 1
--     tt = GetTime()
--     if kk == 71 then kk = 0 end
--   end
-- end)

local function moveFrame(self)
  local centerX, centerY = lqGUI:GetCenter()
  local x0, y0 = GetCursorPosition()
  local x, y = x0 / self:GetEffectiveScale() - centerX, y0 / self:GetEffectiveScale() - centerY
  local boundX = (LQ.mainframe.width - LQ.playerframe.width)/2 - 2
  local boundY = (LQ.mainframe.height - LQ.playerframe.height)/2 - 2
  x = abs(x) > boundX and boundX*x/abs(x) or x
  if  y > boundY - 35 then y = boundY - 35
  elseif y < -boundY then y = -boundY end
  self:ClearAllPoints()
  self:SetPoint("CENTER", x, y)
end

local function landFrame(self)
  --self:ClearAllPoints()
  --self:SetPoint("CENTER", x, y)
  self:SetScript("OnUpdate", nil)
end

lqGUI.options = CreateFrame("Frame", nil, lqGUI) -- options
lqGUI.options:Hide()
lqGUI.options.optionsShown = false
lqGUI.options:SetWidth(LQ.mainframe.width-6)
lqGUI.options:SetHeight(100)
lqGUI.options:SetPoint("TOP", 0, 100)
lqGUI.options:SetBackdrop(backdrop)
lqGUI.options:SetBackdropColor(0,0,0,.85)

lqGUI.titlebar = CreateFrame("Frame", nil, lqGUI) -- Background
lqGUI.titlebar:ClearAllPoints()
lqGUI.titlebar:SetWidth(LQ.mainframe.width-6)
lqGUI.titlebar:SetHeight(35)
lqGUI.titlebar:SetPoint("TOP", 0, -3)
lqGUI.titlebar:SetBackdrop(backdrop_noborder)
lqGUI.titlebar:SetBackdropColor(1,1,1,.10)

lqGUI.text = lqGUI:CreateFontString("Status", "LOW", "GameFontNormal") -- Title
lqGUI.text:ClearAllPoints()
lqGUI.text:SetPoint("TOPLEFT", 12, -12)
lqGUI.text:SetFontObject(GameFontWhite)
lqGUI.text:SetFont(STANDARD_TEXT_FONT, 16, "OUTLINE")
lqGUI.text:SetText("|c00ff0000Lanathel |cffffffffQueue |cff555555" .. LQ.Version .. "|r")
    
lqGUI.closeButton = CreateFrame("Button", nil, lqGUI) -- Exit
lqGUI.closeButton:SetWidth(20)
lqGUI.closeButton:SetHeight(20)
lqGUI.closeButton:SetNormalTexture(makeTexture(lqGUI.closeButton, "Interface\\AddOns\\LanathelQueue\\img\\close_button_up"))
lqGUI.closeButton:SetPushedTexture(makeTexture(lqGUI.closeButton, "Interface\\AddOns\\LanathelQueue\\img\\close_button_down"))
lqGUI.closeButton:SetHighlightTexture(makeTexture(lqGUI.closeButton, "Interface\\AddOns\\LanathelQueue\\img\\button_highlight", "ADD"))
lqGUI.closeButton:SetPoint("TOPRIGHT", -10,-10)
lqGUI.closeButton:SetScript("OnClick", function()
    lqGUI:Hide()
    lqGUI.options:Hide()
    lqGUI.options.optionsShown = false
    lqGUI.isOpen = false
  end)
  
lqGUI.optionsButton = CreateFrame("Button", nil, lqGUI) -- Options
lqGUI.optionsButton:SetWidth(20)
lqGUI.optionsButton:SetHeight(20)
lqGUI.optionsButton:SetNormalTexture(makeTexture(lqGUI.optionsButton, "Interface\\AddOns\\LanathelQueue\\img\\settings_up"))
lqGUI.optionsButton:SetPushedTexture(makeTexture(lqGUI.optionsButton, "Interface\\AddOns\\LanathelQueue\\img\\settings_down"))
lqGUI.optionsButton:SetHighlightTexture(makeTexture(lqGUI.optionsButton, "Interface\\AddOns\\LanathelQueue\\img\\button_highlight", "ADD"))
lqGUI.optionsButton:SetPoint("TOPRIGHT", -40,-10)
lqGUI.optionsButton:SetScript("OnClick", function()
    if lqGUI.options.optionsShown then
        lqGUI.options:Hide()
        lqGUI.options.optionsShown = false
    else
        lqGUI.options:Show()
        lqGUI.options.optionsShown = true
    end
  end)


  lqGUI.options.Diff10 = CreateFrame("CheckButton", "DiffCheckButton10", lqGUI.options, "OptionsCheckButtonTemplate")
  lqGUI.options.Diff10:SetPoint("TOPLEFT", 20, -30)
  getglobal(lqGUI.options.Diff10:GetName() .. 'Text'):SetText("|cffffffff" .. "10 players")
  lqGUI.options.Diff10:SetScript("OnClick", function(self)
    lqGUI.options.Diff10:SetChecked(true)
    lqGUI.options.Diff25:SetChecked(false)
    LQ.Options.diff = 3
    for i = (2^3)+1, 2^4 do
      LQ.Options.priorityList[i] = nil
    end
    lqGUI:UpdatePositions()
    end)

  lqGUI.options.Diff25 = CreateFrame("CheckButton", "DiffCheckButton25", lqGUI.options, "OptionsCheckButtonTemplate")
  lqGUI.options.Diff25:SetPoint("TOPLEFT", 20, -50)
  getglobal(lqGUI.options.Diff25:GetName() .. 'Text'):SetText("|cffffffff" .. "25 players")
  lqGUI.options.Diff25:SetScript("OnClick", function(self)
    lqGUI.options.Diff10:SetChecked(false)
    lqGUI.options.Diff25:SetChecked(true)
    LQ.Options.diff = 4
    local extra = lqGUI.resFuncs[LQ.Options.dataSource]()
    for i = (2^3)+1, 2^4 do
      LQ.Options.priorityList[i] = extra[i] or ("Empty" .. i)
    end
    lqGUI:UpdatePositions()
    end)

  lqGUI.options.source = CreateFrame("CheckButton", "SourceCheckButton", lqGUI.options, "OptionsCheckButtonTemplate")
  lqGUI.options.source:SetPoint("TOPLEFT", 200, -10)
  getglobal(lqGUI.options.source:GetName() .. 'Text'):SetText("|cffffffff" .. "Source:")
  lqGUI.options.source:SetScript("OnClick", function(self)
    LQ.Options.dataSource = "auto"
    if self:GetChecked() then
      lqGUI.options.sourceRecount:Show()
      lqGUI.options.sourceDetails:Show()
      lqGUI.options.sourceSkada:Show()
    else
      lqGUI.options.sourceRecount:Hide()
      lqGUI.options.sourceDetails:Hide()
      lqGUI.options.sourceSkada:Hide()
    end
    lqGUI.options.sourceRecount:SetChecked(false)
    lqGUI.options.sourceDetails:SetChecked(false)
    lqGUI.options.sourceSkada:SetChecked(false)
    end)

  lqGUI.options.sourceRecount = CreateFrame("CheckButton", "SourceRecountCheckButton", lqGUI.options, "OptionsCheckButtonTemplate")
  lqGUI.options.sourceRecount:SetPoint("TOPLEFT", 220, -30)
  getglobal(lqGUI.options.sourceRecount:GetName() .. 'Text'):SetText("|cffffffff" .. "Recount")
  lqGUI.options.sourceRecount:SetScript("OnClick", function(self)
    if Recount then
      LQ.Options.dataSource = "Recount"
      lqGUI.options.sourceRecount:SetChecked(true)
      lqGUI.options.sourceDetails:SetChecked(false)
      lqGUI.options.sourceSkada:SetChecked(false)
    else
      AddMsg("Recount not found")
      lqGUI.options.sourceRecount:SetChecked(false)
    end
    end)
  lqGUI.options.sourceDetails = CreateFrame("CheckButton", "SourceDetailsCheckButton", lqGUI.options, "OptionsCheckButtonTemplate")
  lqGUI.options.sourceDetails:SetPoint("TOPLEFT", 220, -50)
  getglobal(lqGUI.options.sourceDetails:GetName() .. 'Text'):SetText("|cffffffff" .. "Details")
  lqGUI.options.sourceDetails:SetScript("OnClick", function(self)
    if Details then
      LQ.Options.dataSource = "Details"
      lqGUI.options.sourceRecount:SetChecked(false)
      lqGUI.options.sourceDetails:SetChecked(true)
      lqGUI.options.sourceSkada:SetChecked(false)
    else
      AddMsg("Details not found")
      lqGUI.options.sourceDetails:SetChecked(false)
    end
    end)
  lqGUI.options.sourceSkada = CreateFrame("CheckButton", "SourceSkadaCheckButton", lqGUI.options, "OptionsCheckButtonTemplate")
  lqGUI.options.sourceSkada:SetPoint("TOPLEFT", 220, -70)
  getglobal(lqGUI.options.sourceSkada:GetName() .. 'Text'):SetText("|cffffffff" .. "Skada")
  lqGUI.options.sourceSkada:SetScript("OnClick", function(self)
    if Skada then
      LQ.Options.dataSource = "Skada"
      lqGUI.options.sourceRecount:SetChecked(false)
      lqGUI.options.sourceDetails:SetChecked(false)
      lqGUI.options.sourceSkada:SetChecked(true)
    else
      AddMsg("Skada not found")
      lqGUI.options.sourceSkada:SetChecked(false)
    end
    end)

lqGUI.syncButton = CreateFrame("Button", nil, lqGUI) -- Reset
lqGUI.syncButton:SetWidth(20)
lqGUI.syncButton:SetHeight(20)
lqGUI.syncButton:SetNormalTexture(makeTexture(lqGUI.syncButton, "Interface\\AddOns\\LanathelQueue\\img\\sync_up"))
lqGUI.syncButton:SetPushedTexture(makeTexture(lqGUI.syncButton, "Interface\\AddOns\\LanathelQueue\\img\\sync_down"))
lqGUI.syncButton:SetHighlightTexture(makeTexture(lqGUI.syncButton, "Interface\\AddOns\\LanathelQueue\\img\\button_highlight", "ADD"))
lqGUI.syncButton:SetPoint("TOPRIGHT", -70,-10)
lqGUI.syncButton:SetScript("OnClick", function()
    lqGUI:GetDefaultPositions()
  end)
  
lqGUI.reportButton = CreateFrame("Button", nil, lqGUI) -- Report
lqGUI.reportButton:SetWidth(20)
lqGUI.reportButton:SetHeight(20)
lqGUI.reportButton:SetNormalTexture(makeTexture(lqGUI.reportButton, "Interface\\AddOns\\LanathelQueue\\img\\report_up"))
lqGUI.reportButton:SetPushedTexture(makeTexture(lqGUI.reportButton, "Interface\\AddOns\\LanathelQueue\\img\\report_down"))
lqGUI.reportButton:SetHighlightTexture(makeTexture(lqGUI.reportButton, "Interface\\AddOns\\LanathelQueue\\img\\button_highlight", "ADD"))
lqGUI.reportButton:SetPoint("TOPRIGHT", -100,-10)
lqGUI.reportButton:SetScript("OnClick", function()
    lqGUI:Report()
  end)
  
----- Grid -----

do
lqGUI.cells = {}
lqGUI.arrows = {}
lqGUI.cFrames = {}
lqGUI.extraFrames = {}
lqGUI.textures = {}
local num = 2
local x0, y0 = 10, -50
local x, y = x0, y0
for i = 1, 4 do
  lqGUI.cells[i] = {}
  lqGUI.arrows[i] = {}
  lqGUI.cFrames[i] ={}
  for j = 1, num do
  if x > LQ.mainframe.width/2 and j%2 ~= 0 then
    x = x0
    y = y - LQ.playerframe.height - 15
  end
  lqGUI.cells[i][j] = lqGUI:CreateTexture()
  lqGUI.cells[i][j]:SetWidth(LQ.playerframe.width+2)
  lqGUI.cells[i][j]:SetHeight(LQ.playerframe.height+2)
  lqGUI.cells[i][j]:SetPoint("TOPLEFT", x, y)
  lqGUI.cells[i][j]:SetTexture("Interface\\AddOns\\LanathelQueue\\img\\Minimalist")
  lqGUI.cells[i][j]:SetAlpha(0.1)
  
  if j%2 ~= 0 then 
  lqGUI.arrows[i][j] = lqGUI:CreateTexture()
  lqGUI.arrows[i][j]:SetWidth(20)
  lqGUI.arrows[i][j]:SetHeight(15)
  lqGUI.arrows[i][j]:SetPoint("TOPLEFT", x + LQ.playerframe.width+2, y)
  lqGUI.arrows[i][j]:SetTexture("Interface\\AddOns\\LanathelQueue\\img\\Harrow")
  lqGUI.arrows[i][j]:SetAlpha(1)
  end
  
  lqGUI.cFrames[i][j] = CreateFrame("Button", nil, lqGUI)
  lqGUI.cFrames[i][j].coords = {l = i, d = j}
  lqGUI.cFrames[i][j]:SetWidth(LQ.playerframe.width)
  lqGUI.cFrames[i][j]:SetHeight(LQ.playerframe.height)
  lqGUI.cFrames[i][j]:SetNormalTexture(makeTexture(lqGUI.cFrames[i][j], "Interface\\AddOns\\LanathelQueue\\img\\Minimalist"))
  lqGUI.cFrames[i][j]:SetPushedTexture(makeTexture(lqGUI.cFrames[i][j], "Interface\\AddOns\\LanathelQueue\\img\\Minimalist"))
  lqGUI.cFrames[i][j]:SetHighlightTexture(makeTexture(lqGUI.cFrames[i][j], "Interface\\AddOns\\LanathelQueue\\img\\button_highlight", "ADD"))
  lqGUI.cFrames[i][j]:SetPoint("TOPLEFT", x, y)
  lqGUI.cFrames[i][j].initPos = {x0 = x, y0 = y}
  lqGUI.cFrames[i][j]:SetScript("OnClick", function()
  end)
  lqGUI.cFrames[i][j]:SetMovable(true)
  lqGUI.cFrames[i][j]:EnableMouse(true)
  lqGUI.cFrames[i][j]:SetScript("OnMouseDown",function(self)
    self:SetAlpha(0.5)
    self:SetScript("OnUpdate", moveFrame)
  end)
  lqGUI.cFrames[i][j]:SetScript("OnMouseUp",function(self)
    local prox, ip, jp = lqGUI:Proximity(self.text:GetText(), self:GetPoint())
    self:SetScript("OnUpdate", landFrame) -- slow animation TODO
    if prox then
      local i1 = (j%2 == 0) and j/2 + 2^(i-1) or (j+1)/2
      if jp then
      local i2 = (jp%2 == 0) and jp/2 + 2^(ip-1) or (jp+1)/2
      local seco = LQ.Options.priorityList[i2]
      LQ.Options.priorityList[i2] = LQ.Options.priorityList[i1]
      LQ.Options.priorityList[i1] = seco
      else
      LQ.Options.priorityList[i1] = leftoverList[ip]
      table.remove(leftoverList, ip)
      lqGUI.extraFrames[ip]:Hide()
    end
    end
    lqGUI:UpdateRoster()
    lqGUI:UpdatePositions()
    self:SetPoint("TOPLEFT", self.initPos.x0, self.initPos.y0)
    self:SetAlpha(1)
  end)
  lqGUI.cFrames[i][j].alarm = CreateFrame("Button", nil, lqGUI.cFrames[i][j])
  lqGUI.cFrames[i][j].alarm:SetWidth(15)
  lqGUI.cFrames[i][j].alarm:SetHeight(15)
  lqGUI.cFrames[i][j].alarm:SetPoint("LEFT", 0, 8)
  lqGUI.cFrames[i][j].alarm:SetNormalTexture("Interface\\AddOns\\LanathelQueue\\img\\excl")
  lqGUI.cFrames[i][j].alarm:Hide()
  
  lqGUI.cFrames[i][j].text = lqGUI.cFrames[i][j]:CreateFontString("Status", "LOW", "GameFontNormal")
  lqGUI.cFrames[i][j].text:ClearAllPoints()
  lqGUI.cFrames[i][j].text:SetPoint("LEFT", 5, 1)
  lqGUI.cFrames[i][j].text:SetFontObject(GameFontWhite)
  lqGUI.cFrames[i][j].text:SetFont(STANDARD_TEXT_FONT, 14, "OUTLINE")
  lqGUI.cFrames[i][j].text:SetAllPoints(lqGUI.cFrames[i][j])
  lqGUI.cFrames[i][j]:Hide()
  
  x = x + LQ.playerframe.width + 25
  end
  x = x0
  y = y - LQ.playerframe.height - 30
  num = num*2
end

lqGUI.reserve = CreateFrame("Frame", "ReserveScrollFrame", lqGUI)
lqGUI.reserve:SetWidth(LQ.playerframe.width+2)
lqGUI.reserve:SetHeight((LQ.playerframe.height+2)*15)
lqGUI.reserve:SetPoint("TOPRIGHT", -12, -63)
lqGUI.reserve:EnableMouseWheel(true)
lqGUI.reserve:SetScript("OnMouseWheel", function(self, offset)
  if #leftoverList > 17 then
    table.insert(leftoverList, (offset==1) and 1 or #leftoverList, table.remove(leftoverList, (offset==1) and #leftoverList or 1) )
    lqGUI:UpdatePositions()
  end
  end)

lqGUI.reserveCell = lqGUI.reserve:CreateTexture()
lqGUI.reserveCell:SetWidth(LQ.playerframe.width+2)
lqGUI.reserveCell:SetHeight((LQ.playerframe.height+2)*15)
lqGUI.reserveCell:SetPoint("TOPRIGHT", 0, 0)
lqGUI.reserveCell:SetTexture("Interface\\AddOns\\LanathelQueue\\img\\Minimalist")
lqGUI.reserveCell:SetAlpha(0.1)

lqGUI.reserveCell.text = lqGUI:CreateFontString("Status", "LOW", "GameFontNormal")
lqGUI.reserveCell.text:ClearAllPoints()
lqGUI.reserveCell.text:SetPoint("TOPRIGHT", -13, -40)
lqGUI.reserveCell.text:SetFontObject(GameFontWhite)
lqGUI.reserveCell.text:SetFont(STANDARD_TEXT_FONT, 14, "OUTLINE")
lqGUI.reserveCell.text:SetText("Reserve")

lqGUI.vertic = lqGUI:CreateTexture()
lqGUI.vertic:SetWidth(2)
lqGUI.vertic:SetHeight(lqGUI:GetHeight()-40)
lqGUI.vertic:SetPoint("TOPRIGHT", -20-LQ.playerframe.width-5, -40)
lqGUI.vertic:SetTexture("Interface\\AddOns\\LanathelQueue\\img\\Minimalist")
lqGUI.vertic:SetAlpha(0.8)

lqGUI.horiz1 = lqGUI:CreateTexture()
lqGUI.horiz1:SetWidth(LQ.mainframe.width-20-LQ.playerframe.width-5)
lqGUI.horiz1:SetHeight(2)
lqGUI.horiz1:SetPoint("TOPLEFT", 0, -80)
lqGUI.horiz1:SetTexture("Interface\\AddOns\\LanathelQueue\\img\\Minimalist")
lqGUI.horiz1:SetAlpha(0.8)

lqGUI.horiz2 = lqGUI:CreateTexture()
lqGUI.horiz2:SetWidth(LQ.mainframe.width-20-LQ.playerframe.width-5)
lqGUI.horiz2:SetHeight(2)
lqGUI.horiz2:SetPoint("TOPLEFT", 0, -80- LQ.playerframe.height - 30)
lqGUI.horiz2:SetTexture("Interface\\AddOns\\LanathelQueue\\img\\Minimalist")
lqGUI.horiz2:SetAlpha(0.8)

lqGUI.horiz3 = lqGUI:CreateTexture()
lqGUI.horiz3:SetWidth(LQ.mainframe.width-20-LQ.playerframe.width-5)
lqGUI.horiz3:SetHeight(2)
lqGUI.horiz3:SetPoint("TOPLEFT", 0, -110- 3*LQ.playerframe.height - 30-15)
lqGUI.horiz3:SetTexture("Interface\\AddOns\\LanathelQueue\\img\\Minimalist")
lqGUI.horiz3:SetAlpha(0.8)

x, y = -12, -63
for i = 1, 17 do 
  lqGUI.extraFrames[i] = CreateFrame("Button", nil, lqGUI)
  lqGUI.extraFrames[i]:SetWidth(LQ.playerframe.width)
  lqGUI.extraFrames[i]:SetHeight(LQ.playerframe.height)
  lqGUI.extraFrames[i]:SetNormalTexture(makeTexture(lqGUI.extraFrames[i], "Interface\\AddOns\\LanathelQueue\\img\\Minimalist"))
  lqGUI.extraFrames[i]:SetPushedTexture(makeTexture(lqGUI.extraFrames[i], "Interface\\AddOns\\LanathelQueue\\img\\Minimalist"))
  lqGUI.extraFrames[i]:SetHighlightTexture(makeTexture(lqGUI.extraFrames[i], "Interface\\AddOns\\LanathelQueue\\img\\button_highlight", "ADD"))
  lqGUI.extraFrames[i]:SetPoint("TOPRIGHT", x, y)
  lqGUI.extraFrames[i].initPos = {x0 = x, y0 = y}
  lqGUI.extraFrames[i]:SetScript("OnClick", function() end)
  lqGUI.extraFrames[i]:SetMovable(true)
  lqGUI.extraFrames[i]:EnableMouse(true)
  lqGUI.extraFrames[i]:SetScript("OnMouseDown",function(self)
    self:SetAlpha(0.5)
    self:SetScript("OnUpdate", moveFrame)
  end)
  lqGUI.extraFrames[i]:SetScript("OnMouseUp",function(self)
    local prox, ip, jp = lqGUI:Proximity(self.text:GetText(), self:GetPoint())
    self:SetScript("OnUpdate", landFrame) -- slow animation TODO
    if prox and jp then
      local i2 = (jp%2 == 0) and jp/2 + 2^(ip-1) or (jp+1)/2
      LQ.Options.priorityList[i2] = leftoverList[i]
      table.remove(leftoverList, i)
      lqGUI.extraFrames[i]:Hide()
    end
    lqGUI:UpdateRoster()
    lqGUI:UpdatePositions()
    self:SetPoint("TOPRIGHT", self.initPos.x0, self.initPos.y0)
    self:SetAlpha(1)
  end)
  
  lqGUI.extraFrames[i].text = lqGUI.extraFrames[i]:CreateFontString("Status", "LOW", "GameFontNormal")
  lqGUI.extraFrames[i].text:ClearAllPoints()
  lqGUI.extraFrames[i].text:SetPoint("LEFT", 5, 1)
  lqGUI.extraFrames[i].text:SetFontObject(GameFontWhite)
  lqGUI.extraFrames[i].text:SetFont(STANDARD_TEXT_FONT, 14, "OUTLINE")
  lqGUI.extraFrames[i].text:SetAllPoints(lqGUI.extraFrames[i])
  lqGUI.extraFrames[i]:Hide()
  y = y - LQ.playerframe.height
end
end

----- Frames ---

function lqGUI:AssignCharToFrame(name, i, j)
  local c = raid[name] and RAID_CLASS_COLORS[raid[name].class] or {r=0.5,g=0.5,b=0.5}
  self.cFrames[i][j]:GetNormalTexture():SetTexture(c.r, c.g, c.b)
  self.cFrames[i][j]:GetPushedTexture():SetTexture(c.r, c.g, c.b)
  self.cFrames[i][j].text:SetText(name)
  self.cFrames[i][j]:Show()
  self.cFrames[i][j].alarm:Hide()
  if not raid[name] then
    self.cFrames[i][j].alarm:Show()
  end
end

function lqGUI:AssignCharToReserve(name, i)
  local c = raid[name] and RAID_CLASS_COLORS[raid[name].class] or {r=1,g=1,b=1}
  self.extraFrames[i]:GetNormalTexture():SetTexture(c.r, c.g, c.b)
  self.extraFrames[i]:GetPushedTexture():SetTexture(c.r, c.g, c.b)
  self.extraFrames[i].text:SetText(name)
  self.extraFrames[i]:Show()
end

function lqGUI:UpdateRoster()
  table.wipe(raid)
  table.wipe(leftoverList)
  for i = 1, GetNumRaidMembers() do
    local name, rank, _, _, _, fileName, _, online, isDead = GetRaidRosterInfo(i)
    --local name, fileName = "Test" .. i, "DRUID"
    if name then
      raid[name] = raid[name] or {}
      raid[name].name = name
      raid[name].rank = rank
      raid[name].online = online
      raid[name].isDead = isDead
      raid[name].class = fileName
      raid[name].id = "raid"..i
      if not checkEntry(LQ.Options.priorityList, name) then
        leftoverList[#leftoverList+1] = name
      end
    end
  end
end

function lqGUI:HideFrames()
  for i = 1, 4 do
    for j = 1, 2^i do
      self.cFrames[i][j]:Hide()
    end
  end
  for i = 1, 17 do
    self.extraFrames[i]:Hide()
  end
end

function lqGUI:UpdatePositions()
  self:HideFrames()
  local num = 1
  for i = 1, LQ.Options.diff do
    for j = 1, num do
      self:AssignCharToFrame(LQ.Options.priorityList[j], i, 2*j-1)
      self:AssignCharToFrame(LQ.Options.priorityList[j+num], i, 2*j)
    end
    num = num*2
  end
  for i,v in ipairs(leftoverList) do
    if i<=17 then
      self:AssignCharToReserve(v, i)
    end
  end
end

function lqGUI:Report()
  local num = 1
  local str = ""
  for i = 1, LQ.Options.diff do
    str = ""
    for j = 1, num do
      str = str .. (LQ.Options.priorityList[j] or "Empty" .. j) .. " - " .. (LQ.Options.priorityList[j + num] or "Empty" .. (j + num)) .. " / " 
    end
    num = num*2
    if GetNumRaidMembers() > 0 then 
      SendChatMessage(str, "RAID")
    else
      AddMsg(str)
    end
  end
end

function lqGUI:GetDefaultPositions()
  LQ.Options.priorityList = lqGUI.resFuncs[LQ.Options.dataSource]()
  while #LQ.Options.priorityList < 2^LQ.Options.diff do
    LQ.Options.priorityList[#LQ.Options.priorityList+1] = "Empty" .. (#LQ.Options.priorityList + 1)
  end
  while #LQ.Options.priorityList > 2^LQ.Options.diff do
    table.remove(LQ.Options.priorityList, #LQ.Options.priorityList)
  end
  self:UpdateRoster()
  self:UpdatePositions()
end

function lqGUI:Proximity(name, ...)
  local num = 2
  local x, y = select(4, ...)
  x, y = x + LQ.mainframe.width/2, y - LQ.mainframe.height/2 
  local dist, td = {}, {}
  for i = 1, LQ.Options.diff do
    for j = 1, num do
    --if lqGUI.cFrames[i][j].text:GetText() == name then break end 
    local x1, y1 = select(4, self.cFrames[i][j]:GetPoint())
    x1, y1 = x1 + LQ.playerframe.width/2 , y1 - LQ.playerframe.height/2
    local s = (x1 - x)^2+(y1-y)^2
    if s < 200 then
      dist[s] = {i, j}
      td[#td + 1] = s
    end
  end
  num = num*2
  end
  for i = 1, 17 do 
   if self.extraFrames[i]:IsVisible() then
    local x1, y1 = select(4, self.extraFrames[i]:GetPoint())
  x1, y1 = x1 + LQ.mainframe.width - LQ.playerframe.width/2 , y1 - LQ.playerframe.height/2
  local s = (x1 - x)^2+(y1-y)^2
  if s < 200 then
    dist[s] = {i}
    td[#td + 1] = s
  end
   end
  end
  table.sort(td)
  if td[1] then 
    return td[1], dist[td[1]][1], dist[td[1]][2] 
  else 
    return nil 
  end
end

SLASH_LQ1 = "/lq"
SlashCmdList["LQ"] = function(msg)
  lqGUI:Show()
end
---------------------
-- Minimap Button  --
---------------------
do
    
  local function moveButton(self)
      local minimapShape = _G.GetMinimapShape and _G.GetMinimapShape() or 'ROUND'
      local centerX, centerY = Minimap:GetCenter()
      local x, y = GetCursorPosition()
      x, y = x / self:GetEffectiveScale() - centerX, y / self:GetEffectiveScale() - centerY
      centerX, centerY = math.abs(x), math.abs(y)
      local cos , sin = centerX / math.sqrt(centerX^2 + centerY^2) , centerY / sqrt(centerX^2 + centerY^2)
      if minimapShape == "ROUND" then
        centerX, centerY = cos*80, sin*80
      elseif minimapShape == "SQUARE" then
        centerX, centerY = math.max(-82, math.min(110*cos, 84)) , math.max(-86, math.min(110*sin, 82))
      end
      centerX = x < 0 and -1.4*centerX or 1.4*centerX
      centerY = y < 0 and -1.4*centerY or 1.4*centerY
      self:ClearAllPoints()
      LQ.Options.minimapPos = LQ.Options.minimapPos or {}
      LQ.Options.minimapPos.x, LQ.Options.minimapPos.y  = centerX, centerY
      self:SetPoint("CENTER", centerX, centerY)
  end
    
  lqGUI.button = CreateFrame("Button", "JMinimapButton", Minimap)
  lqGUI.button:SetHeight(32)
  lqGUI.button:SetWidth(32)
  lqGUI.button:SetFrameStrata("MEDIUM")
  lqGUI.button:SetPoint("CENTER", 0 ,0)
  lqGUI.button:SetMovable(true)
  lqGUI.button:SetUserPlaced(true)
  lqGUI.button:SetNormalTexture("Interface\\AddOns\\LanathelQueue\\img\\MinimapButtonNormal")
  lqGUI.button:SetPushedTexture("Interface\\AddOns\\LanathelQueue\\img\\MinimapButtonPushed")
  lqGUI.button:SetHighlightTexture("Interface\\Minimap\\UI-Minimap-ZoomButton-Highlight")
  lqGUI.button:SetScript("OnMouseDown", function(self, button)
    if IsShiftKeyDown() then
        self:SetScript("OnUpdate", moveButton)
    end
  end)
  lqGUI.button:SetScript("OnMouseUp", function(self)
      self:SetScript("OnUpdate", nil)
    end)
  lqGUI.button:SetScript("OnClick", function(self, button)
      if lqGUI.isOpen and not IsShiftKeyDown() then
        lqGUI:Hide()
        lqGUI.isOpen = false
      elseif not IsShiftKeyDown() then
        lqGUI:Show()
        lqGUI.isOpen = true
      end
    end)
        
  lqGUI.options.minimapCB = CreateFrame("CheckButton", "minimapIconCheckButton", lqGUI.options, "OptionsCheckButtonTemplate")
  lqGUI.options.minimapCB:SetPoint("TOPLEFT", 20, -10)
  getglobal(lqGUI.options.minimapCB:GetName() .. 'Text'):SetText("|cffffffff" .. "Minimap button")
  lqGUI.options.minimapCB:SetScript("OnClick", function(self)
      if self:GetChecked() then
        lqGUI.button:Show()
        LQ.Options.minimapIcon = true
      else
        lqGUI.button:Hide()
        LQ.Options.minimapIcon = false
      end
    end)
end